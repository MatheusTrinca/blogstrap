class Comment < ApplicationRecord
  # tabela comments tem o article_id
  belongs_to :article 
  # tabela comments tem o user_id
  belongs_to :user

  validates :body, presence: true
end

# 18:20