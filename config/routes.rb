Rails.application.routes.draw do
  scope '(:locale)', locale: /pt-BR|en/ do
    devise_for :users
    root 'articles#index'
    
    resources :articles do
      # /articles/1/comments/2
      resources :comments, only: %i[create destroy]
    end
  
    resources :categories, except: [:show]
  end
end 
